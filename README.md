# calendarify

Toy project. I'd like to expose some of the services I use in calendar form (as `vtodo`s or `vevent`s).

e.g. Bitbucket issues as `vtodo`s.

## Howto

- Checkout
- Get git submodules
- Build using [Bazel](http://bazel.io).

## Status

Present some issues from a hardcoded project as a caldav file. Not very
rich in content, either, yet.

No progress on caldav.
