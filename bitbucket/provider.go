package bitbucket

import (
	"badc0de.net/pkg/calendarify/ical"
	api "bitbucket.org/api/gen"
	"errors"
	"strconv"
)

type BitbucketVTodo struct {
	id       int32
	title    string
	status   string
	priority int32
	repoUuid string
}

type BitbucketProvider struct {
	Config *api.Configuration

	User, Project string
}

func NewBitbucketProvider(user, project string) *BitbucketProvider {
	bbp := &BitbucketProvider{
		User:    user,
		Project: project,
	}

	cfg := api.NewConfiguration()
	cfg.UserAgent = "badc0de-calendarify/0.0.1"

	bbp.Config = cfg
	return bbp
}

func (bbp *BitbucketProvider) AllVTodos() ([]ical.VTodo, error) {
	it := api.NewIssuetrackerApi()
	it.Configuration = bbp.Config

	if it.Configuration.AccessToken == "" {
		return nil, errors.New("No access token")
	}
	// skipping api response value
	paginated, _, err := it.RepositoriesUsernameRepoSlugIssuesGet(bbp.User, bbp.Project)
	if err != nil {
		return nil, err
	}

	vtodos := make([]ical.VTodo, len(*paginated.Values), len(*paginated.Values))
	for idx, val := range *paginated.Values {
		var state string
		switch *val.State {
		case "new":
			state = "NEEDS-ACTION"
		case "open":
			state = "IN-PROCESS"
		case "resolved":
			state = "COMPLETED"
		case "on hold":
			state = "NEEDS-ACTION"
		case "invalid":
			state = "CANCELLED"
		case "duplicate":
			state = "CANCELLED"
		case "wontfix":
			state = "CANCELLED"
		case "closed":
			state = "COMPLETED"
		}
		var priority int32
		switch *val.Priority {
		case "trivial":
			priority = 5
		case "minor":
			priority = 4
		case "major":
			priority = 3
		case "critical":
			priority = 2
		case "blocker":
			priority = 1
		}
		vtodos[idx] = &BitbucketVTodo{
			id:       *val.Id,
			title:    *val.Title,
			status:   state,
			priority: priority,
			repoUuid: *val.Repository.Uuid,
		}
	}

	return vtodos, nil
}

func (vt BitbucketVTodo) GetId() string {
	return strconv.Itoa(int(vt.id))
}
func (vt BitbucketVTodo) GetSummary() string {
	return vt.title
}
func (vt BitbucketVTodo) GetStatus() string {
	return vt.status
}
func (vt BitbucketVTodo) GetPriority() string {
	return strconv.Itoa(int(vt.priority))
}

func (vt BitbucketVTodo) GetUID() string {
	return vt.repoUuid[1:len(vt.repoUuid)-1] + "." + vt.GetId() + "@issues.bitbucket.org.calendarify.badc0de.net"
}

func (vt BitbucketVTodo) MarshalText() ([]byte, error) {
	return ical.DefaultVTodoMarshaler{vt}.MarshalText()
}
