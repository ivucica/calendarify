package main

import (
	"log"
	"net/http"

	"golang.org/x/net/webdav"
)

type httpHandler struct{}

func (httpHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
}

func init() {
	//flag.Parse()
}

func main() {
	//http.Handle("/", &httpHandler{})

	dav := &webdav.Handler{}
	dav.FileSystem = webdav.NewMemFS()
	dav.LockSystem = webdav.NewMemLS()
	dav.Logger = func(req *http.Request, err error) {
		log.Println(req.Method + " " + req.URL.Path)
	}
	dav.Prefix = "/"
	http.Handle("/", dav)
	
	http.ListenAndServe(":9898", nil)
}
