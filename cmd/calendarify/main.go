package main

import (
	"badc0de.net/pkg/calendarify/bitbucket"
	"bytes"
	"encoding/base64"
	"encoding/gob"
	"flag"
	"fmt"
	"golang.org/x/oauth2"
	bitbucket_oauth2 "golang.org/x/oauth2/bitbucket"
	"net/http"
	"net/url"
)

var (
	apiKey           = flag.String("bitbucket_api_key", "", "api key used to access bitbucket api")
	apiSecret        = flag.String("bitbucket_api_secret", "", "api secret used to access bitbucket api")
	oauthStateString = ")ERHopdnsfoprope"

	user    = flag.String("bitbucket_user", "ivucica", "owner of the project for which todos should be served")
	project = flag.String("bitbucket_project", "calendarify", "project for which todos should be served")

	debugContentType = flag.String("debug_content_type", "text/calendar", "debug option: which content type to serve todos under")
)

type httpHandler struct{}

func (httpHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	oauthConf := oauth2.Config{
		ClientID:     *apiKey,
		ClientSecret: *apiSecret,
		Endpoint:     bitbucket_oauth2.Endpoint,
		RedirectURL:  "http://badc0de.net:9898/oauth2_cb",
		Scopes: []string{
			"issue",
		},
	}
	if r.RequestURI == "/" {
		//w.Header().Add("Content-type", "text/html")
		//w.Write([]byte("<form method='post'><input name='who'><input type='submit'></form>"))

		url := oauthConf.AuthCodeURL(oauthStateString, oauth2.AccessTypeOnline)

		http.Redirect(w, r, url, http.StatusTemporaryRedirect)
		return
	}
	if len(r.RequestURI) > len("/oauth2_cb?") && r.RequestURI[0:len("/oauth2_cb?")] == "/oauth2_cb?" {
		state := r.FormValue("state")
		if state != oauthStateString {
			fmt.Printf("invalid oauth state, expected '%s', got '%s'\n", oauthStateString, state)
			http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
			return
		}

		code := r.FormValue("code")
		token, err := oauthConf.Exchange(oauth2.NoContext, code)
		if err != nil {
			fmt.Printf("oauthConf.Exchange() failed with '%s'\n", err)
			http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		}
		//oauthClient := oauthConf.Client(oauth2.NoContext, token)

		var stateBuf bytes.Buffer
		enc := gob.NewEncoder(&stateBuf)
		enc.Encode(*token)
		state64 := base64.StdEncoding.EncodeToString(stateBuf.Bytes())

		v := url.Values{}
		v.Add("state", state64)
		http.Redirect(w, r, "todos.ical?"+v.Encode(), http.StatusTemporaryRedirect)

		return
	}

	if len(r.RequestURI) > len("/todos.ical?state=") && r.RequestURI[0:len("/todos.ical?state=")] == "/todos.ical?state=" {
		u, _ := url.Parse(r.RequestURI)

		state64 := u.Query().Get("state")
		stateBytes, err := base64.StdEncoding.DecodeString(state64)
		if err != nil {
			fmt.Printf("base64 decoding: %s (%s)", err.Error(), state64)
			http.Error(w, "decoding base64 failed", http.StatusInternalServerError) // TODO: actually bad request
			return
		}
		state := bytes.NewBuffer(stateBytes)
		dec := gob.NewDecoder(state)

		token := &oauth2.Token{}
		dec.Decode(token)

		w.Header().Add("Content-type", *debugContentType)
		provider := bitbucket.NewBitbucketProvider(*user, *project)
		if provider == nil || provider.Config == nil || token == nil {
			w.Write([]byte("Error creating provider"))
			return
		}
		provider.Config.AccessToken = token.AccessToken
		todos, err := provider.AllVTodos()
		if err != nil {
			w.Write([]byte("Error: " + err.Error()))
			return
		}

		w.Write([]byte("BEGIN:VCALENDAR\n"))
		w.Write([]byte("VERSION:2.0\n"))
		w.Write([]byte("PRODID:-//badc0de.net/pkg//NONSGML calendarify early alpha//EN\n"))

		for _, todo := range todos {
			txt, err := todo.MarshalText()
			if err == nil {
				w.Write(txt)
			} else {
				w.Write([]byte("X-ERROR:" + err.Error() + "\n"))
			}
		}
		w.Write([]byte("END:VCALENDAR\n"))
	}
}

func init() {
	flag.Parse()
}

func main() {
	http.Handle("/", &httpHandler{})
	http.ListenAndServe(":9898", nil)
}
