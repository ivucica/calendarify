package ical

import (
	"bytes"
	"encoding"
)

type VTodo interface {
	encoding.TextMarshaler

	GetUID() string
	GetId() string
	GetSummary() string
	GetStatus() string
	GetPriority() string
}

type VTodoProvider interface {
	AllVTodos() []VTodo
}

type DefaultVTodoMarshaler struct{
	VTodo
}

func (todo DefaultVTodoMarshaler) MarshalText() ([]byte, error) {
	w := bytes.Buffer{}
	w.Write([]byte("BEGIN:VTODO\n"))
	w.Write([]byte("UID:" + todo.GetUID() + "\n"))
	w.Write([]byte("SUMMARY:" + todo.GetSummary() + "\n"))
	if todo.GetStatus() != "" {
		w.Write([]byte("STATUS:" + todo.GetStatus() + "\n"))
	}
	if todo.GetPriority() != "0" {
		w.Write([]byte("PRIORITY:" + todo.GetPriority() + "\n"))
	}
	w.Write([]byte("END:VTODO\n"))
	return w.Bytes(), nil
}
