package tokenstore // badc0de.net/pkg/calendarify/tokenstore

import (
	"golang.org/x/oauth2"
)

type TokenStore interface{
	GetToken(User, Service string) oauth2.Token
}
