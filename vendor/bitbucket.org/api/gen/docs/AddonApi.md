# \AddonApi

All URIs are relative to *https://api.bitbucket.org/2.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AccountUsernameAddonsEncodedContextIdDelete**](AddonApi.md#AccountUsernameAddonsEncodedContextIdDelete) | **Delete** /account/{username}/addons/{encoded_context_id} | 
[**AccountUsernameAddonsEncodedContextIdGet**](AddonApi.md#AccountUsernameAddonsEncodedContextIdGet) | **Get** /account/{username}/addons/{encoded_context_id} | 
[**AccountUsernameAddonsEncodedContextIdPut**](AddonApi.md#AccountUsernameAddonsEncodedContextIdPut) | **Put** /account/{username}/addons/{encoded_context_id} | 
[**AccountUsernameAddonsEncodedContextIdRefreshPut**](AddonApi.md#AccountUsernameAddonsEncodedContextIdRefreshPut) | **Put** /account/{username}/addons/{encoded_context_id}/refresh | 
[**AccountUsernameAddonsGet**](AddonApi.md#AccountUsernameAddonsGet) | **Get** /account/{username}/addons | 
[**AccountUsernameAddonsPost**](AddonApi.md#AccountUsernameAddonsPost) | **Post** /account/{username}/addons | 
[**AddonDelete**](AddonApi.md#AddonDelete) | **Delete** /addon | 
[**AddonLinkersGet**](AddonApi.md#AddonLinkersGet) | **Get** /addon/linkers | 
[**AddonLinkersLinkerKeyGet**](AddonApi.md#AddonLinkersLinkerKeyGet) | **Get** /addon/linkers/{linker_key} | 
[**AddonLinkersLinkerKeyValuesDelete**](AddonApi.md#AddonLinkersLinkerKeyValuesDelete) | **Delete** /addon/linkers/{linker_key}/values | 
[**AddonLinkersLinkerKeyValuesDelete_0**](AddonApi.md#AddonLinkersLinkerKeyValuesDelete_0) | **Delete** /addon/linkers/{linker_key}/values/ | 
[**AddonLinkersLinkerKeyValuesGet**](AddonApi.md#AddonLinkersLinkerKeyValuesGet) | **Get** /addon/linkers/{linker_key}/values | 
[**AddonLinkersLinkerKeyValuesGet_0**](AddonApi.md#AddonLinkersLinkerKeyValuesGet_0) | **Get** /addon/linkers/{linker_key}/values/ | 
[**AddonLinkersLinkerKeyValuesPost**](AddonApi.md#AddonLinkersLinkerKeyValuesPost) | **Post** /addon/linkers/{linker_key}/values | 
[**AddonLinkersLinkerKeyValuesPut**](AddonApi.md#AddonLinkersLinkerKeyValuesPut) | **Put** /addon/linkers/{linker_key}/values | 
[**AddonPut**](AddonApi.md#AddonPut) | **Put** /addon | 


# **AccountUsernameAddonsEncodedContextIdDelete**
> ModelError AccountUsernameAddonsEncodedContextIdDelete($username, $encodedContextId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **encodedContextId** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AccountUsernameAddonsEncodedContextIdGet**
> ModelError AccountUsernameAddonsEncodedContextIdGet($username, $encodedContextId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **encodedContextId** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AccountUsernameAddonsEncodedContextIdPut**
> ModelError AccountUsernameAddonsEncodedContextIdPut($username, $encodedContextId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **encodedContextId** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AccountUsernameAddonsEncodedContextIdRefreshPut**
> ModelError AccountUsernameAddonsEncodedContextIdRefreshPut($username, $encodedContextId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **encodedContextId** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AccountUsernameAddonsGet**
> ModelError AccountUsernameAddonsGet($username)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AccountUsernameAddonsPost**
> ModelError AccountUsernameAddonsPost($username)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddonDelete**
> ModelError AddonDelete()






### Parameters
This endpoint does not need any parameter.

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddonLinkersGet**
> ModelError AddonLinkersGet()






### Parameters
This endpoint does not need any parameter.

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddonLinkersLinkerKeyGet**
> ModelError AddonLinkersLinkerKeyGet($linkerKey)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **linkerKey** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddonLinkersLinkerKeyValuesDelete**
> ModelError AddonLinkersLinkerKeyValuesDelete($linkerKey)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **linkerKey** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddonLinkersLinkerKeyValuesDelete_0**
> ModelError AddonLinkersLinkerKeyValuesDelete_0($linkerKey)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **linkerKey** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddonLinkersLinkerKeyValuesGet**
> ModelError AddonLinkersLinkerKeyValuesGet($linkerKey)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **linkerKey** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddonLinkersLinkerKeyValuesGet_0**
> ModelError AddonLinkersLinkerKeyValuesGet_0($linkerKey)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **linkerKey** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddonLinkersLinkerKeyValuesPost**
> ModelError AddonLinkersLinkerKeyValuesPost($linkerKey)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **linkerKey** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddonLinkersLinkerKeyValuesPut**
> ModelError AddonLinkersLinkerKeyValuesPut($linkerKey)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **linkerKey** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AddonPut**
> ModelError AddonPut()






### Parameters
This endpoint does not need any parameter.

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

