# \BranchrestrictionsApi

All URIs are relative to *https://api.bitbucket.org/2.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**RepositoriesUsernameRepoSlugBranchRestrictionsGet**](BranchrestrictionsApi.md#RepositoriesUsernameRepoSlugBranchRestrictionsGet) | **Get** /repositories/{username}/{repo_slug}/branch-restrictions | 
[**RepositoriesUsernameRepoSlugBranchRestrictionsIdDelete**](BranchrestrictionsApi.md#RepositoriesUsernameRepoSlugBranchRestrictionsIdDelete) | **Delete** /repositories/{username}/{repo_slug}/branch-restrictions/{id} | 
[**RepositoriesUsernameRepoSlugBranchRestrictionsIdGet**](BranchrestrictionsApi.md#RepositoriesUsernameRepoSlugBranchRestrictionsIdGet) | **Get** /repositories/{username}/{repo_slug}/branch-restrictions/{id} | 
[**RepositoriesUsernameRepoSlugBranchRestrictionsIdPut**](BranchrestrictionsApi.md#RepositoriesUsernameRepoSlugBranchRestrictionsIdPut) | **Put** /repositories/{username}/{repo_slug}/branch-restrictions/{id} | 
[**RepositoriesUsernameRepoSlugBranchRestrictionsPost**](BranchrestrictionsApi.md#RepositoriesUsernameRepoSlugBranchRestrictionsPost) | **Post** /repositories/{username}/{repo_slug}/branch-restrictions | 


# **RepositoriesUsernameRepoSlugBranchRestrictionsGet**
> PaginatedBranchrestrictions RepositoriesUsernameRepoSlugBranchRestrictionsGet($username, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**PaginatedBranchrestrictions**](paginated_branchrestrictions.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugBranchRestrictionsIdDelete**
> RepositoriesUsernameRepoSlugBranchRestrictionsIdDelete($username, $id)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **id** | **string**| The restriction rule&#39;s id | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugBranchRestrictionsIdGet**
> Branchrestriction RepositoriesUsernameRepoSlugBranchRestrictionsIdGet($username, $id)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **id** | **string**| The restriction rule&#39;s id | 

### Return type

[**Branchrestriction**](branchrestriction.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugBranchRestrictionsIdPut**
> Branchrestriction RepositoriesUsernameRepoSlugBranchRestrictionsIdPut($username, $id, $body)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **id** | **string**| The restriction rule&#39;s id | 
 **body** | [**Branchrestriction**](Branchrestriction.md)| The new version of the existing rule | 

### Return type

[**Branchrestriction**](branchrestriction.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugBranchRestrictionsPost**
> Branchrestriction RepositoriesUsernameRepoSlugBranchRestrictionsPost($username, $repoSlug, $body)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 
 **body** | [**Branchrestriction**](Branchrestriction.md)| The new rule | 

### Return type

[**Branchrestriction**](branchrestriction.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

