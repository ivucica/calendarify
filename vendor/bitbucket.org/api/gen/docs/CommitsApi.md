# \CommitsApi

All URIs are relative to *https://api.bitbucket.org/2.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**RepositoriesUsernameRepoSlugCommitNodeApproveDelete**](CommitsApi.md#RepositoriesUsernameRepoSlugCommitNodeApproveDelete) | **Delete** /repositories/{username}/{repo_slug}/commit/{node}/approve | 
[**RepositoriesUsernameRepoSlugCommitNodeApprovePost**](CommitsApi.md#RepositoriesUsernameRepoSlugCommitNodeApprovePost) | **Post** /repositories/{username}/{repo_slug}/commit/{node}/approve | 
[**RepositoriesUsernameRepoSlugCommitRevisionGet**](CommitsApi.md#RepositoriesUsernameRepoSlugCommitRevisionGet) | **Get** /repositories/{username}/{repo_slug}/commit/{revision} | 
[**RepositoriesUsernameRepoSlugCommitShaCommentsCommentIdGet**](CommitsApi.md#RepositoriesUsernameRepoSlugCommitShaCommentsCommentIdGet) | **Get** /repositories/{username}/{repo_slug}/commit/{sha}/comments/{comment_id} | 
[**RepositoriesUsernameRepoSlugCommitShaCommentsGet**](CommitsApi.md#RepositoriesUsernameRepoSlugCommitShaCommentsGet) | **Get** /repositories/{username}/{repo_slug}/commit/{sha}/comments | 
[**RepositoriesUsernameRepoSlugCommitsGet**](CommitsApi.md#RepositoriesUsernameRepoSlugCommitsGet) | **Get** /repositories/{username}/{repo_slug}/commits | 
[**RepositoriesUsernameRepoSlugCommitsPost**](CommitsApi.md#RepositoriesUsernameRepoSlugCommitsPost) | **Post** /repositories/{username}/{repo_slug}/commits | 
[**RepositoriesUsernameRepoSlugCommitsRevisionGet**](CommitsApi.md#RepositoriesUsernameRepoSlugCommitsRevisionGet) | **Get** /repositories/{username}/{repo_slug}/commits/{revision} | 
[**RepositoriesUsernameRepoSlugCommitsRevisionPost**](CommitsApi.md#RepositoriesUsernameRepoSlugCommitsRevisionPost) | **Post** /repositories/{username}/{repo_slug}/commits/{revision} | 
[**RepositoriesUsernameRepoSlugDiffSpecGet**](CommitsApi.md#RepositoriesUsernameRepoSlugDiffSpecGet) | **Get** /repositories/{username}/{repo_slug}/diff/{spec} | 
[**RepositoriesUsernameRepoSlugPatchSpecGet**](CommitsApi.md#RepositoriesUsernameRepoSlugPatchSpecGet) | **Get** /repositories/{username}/{repo_slug}/patch/{spec} | 


# **RepositoriesUsernameRepoSlugCommitNodeApproveDelete**
> RepositoriesUsernameRepoSlugCommitNodeApproveDelete($username, $node)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **node** | **string**| The commit&#39;s SHA1. | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugCommitNodeApprovePost**
> Participant RepositoriesUsernameRepoSlugCommitNodeApprovePost($username, $node)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **node** | **string**| The commit&#39;s SHA1. | 

### Return type

[**Participant**](participant.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugCommitRevisionGet**
> Commit RepositoriesUsernameRepoSlugCommitRevisionGet($username, $revision)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **revision** | **string**| The commit&#39;s SHA1. | 

### Return type

[**Commit**](commit.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugCommitShaCommentsCommentIdGet**
> ModelError RepositoriesUsernameRepoSlugCommitShaCommentsCommentIdGet($username, $sha, $commentId, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **sha** | **string**|  | 
 **commentId** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugCommitShaCommentsGet**
> ModelError RepositoriesUsernameRepoSlugCommitShaCommentsGet($username, $sha, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **sha** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugCommitsGet**
> ModelError RepositoriesUsernameRepoSlugCommitsGet($username, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugCommitsPost**
> ModelError RepositoriesUsernameRepoSlugCommitsPost($username, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugCommitsRevisionGet**
> ModelError RepositoriesUsernameRepoSlugCommitsRevisionGet($username, $revision, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **revision** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugCommitsRevisionPost**
> ModelError RepositoriesUsernameRepoSlugCommitsRevisionPost($username, $revision, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **revision** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugDiffSpecGet**
> RepositoriesUsernameRepoSlugDiffSpecGet($username, $spec, $repoSlug, $context, $path)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **spec** | **string**|  | 
 **repoSlug** | **string**|  | 
 **context** | **int32**| Generate diffs with &lt;n&gt; lines of context instead of the usual three | [optional] 
 **path** | **string**| Limit the diff to a single file | [optional] 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugPatchSpecGet**
> RepositoriesUsernameRepoSlugPatchSpecGet($username, $spec, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **spec** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

