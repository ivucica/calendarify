# Commitstatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type_** | **string** |  | [default to null]
**Uuid** | **string** | The commit status&#39; id. | [optional] [default to null]
**Links** | [**Object**](object.md) |  | [optional] [default to null]
**Url** | **string** | A URL linking back to the vendor or build system, for providing more information about whatever process produced this status | [optional] [default to null]
**Description** | **string** | A description of the build (e.g. \&quot;Unit tests in Bamboo\&quot;) | [optional] [default to null]
**CreatedOn** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**State** | **string** | Provides some indication of the status of this commit | [optional] [default to null]
**Key** | **string** | An identifier for the status that&#39;s unique to         its type (current \&quot;build\&quot; is the only supported type) and the vendor,         e.g. BB-DEPLOY | [optional] [default to null]
**UpdatedOn** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Name** | **string** | An identifier for the build itself, e.g. BB-DEPLOY-1 | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


