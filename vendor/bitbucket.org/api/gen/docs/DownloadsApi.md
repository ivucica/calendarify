# \DownloadsApi

All URIs are relative to *https://api.bitbucket.org/2.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**RepositoriesUsernameRepoSlugDownloadsFilenameDelete**](DownloadsApi.md#RepositoriesUsernameRepoSlugDownloadsFilenameDelete) | **Delete** /repositories/{username}/{repo_slug}/downloads/{filename} | 
[**RepositoriesUsernameRepoSlugDownloadsFilenameGet**](DownloadsApi.md#RepositoriesUsernameRepoSlugDownloadsFilenameGet) | **Get** /repositories/{username}/{repo_slug}/downloads/{filename} | 
[**RepositoriesUsernameRepoSlugDownloadsGet**](DownloadsApi.md#RepositoriesUsernameRepoSlugDownloadsGet) | **Get** /repositories/{username}/{repo_slug}/downloads | 
[**RepositoriesUsernameRepoSlugDownloadsPost**](DownloadsApi.md#RepositoriesUsernameRepoSlugDownloadsPost) | **Post** /repositories/{username}/{repo_slug}/downloads | 


# **RepositoriesUsernameRepoSlugDownloadsFilenameDelete**
> ModelError RepositoriesUsernameRepoSlugDownloadsFilenameDelete($username, $filename, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **filename** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugDownloadsFilenameGet**
> ModelError RepositoriesUsernameRepoSlugDownloadsFilenameGet($username, $filename, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **filename** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugDownloadsGet**
> ModelError RepositoriesUsernameRepoSlugDownloadsGet($username, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugDownloadsPost**
> ModelError RepositoriesUsernameRepoSlugDownloadsPost($username, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

