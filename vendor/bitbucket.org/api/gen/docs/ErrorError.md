# ErrorError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Message** | **string** |  | [default to null]
**Detail** | **string** |  | [optional] [default to null]
**Id** | **string** | A unique identifier for the error for use in bug reports and support cases. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


