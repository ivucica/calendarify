# HookEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Category** | **string** | The category this event belongs to. | [optional] [default to null]
**Event** | **string** | The event identifier. | [optional] [default to null]
**Description** | **string** | More detailed description of the webhook event type. | [optional] [default to null]
**Label** | **string** | Summary of the webhook event type. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


