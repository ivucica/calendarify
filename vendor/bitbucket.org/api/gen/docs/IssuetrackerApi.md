# \IssuetrackerApi

All URIs are relative to *https://api.bitbucket.org/2.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**RepositoriesUsernameRepoSlugComponentsComponentIdGet**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugComponentsComponentIdGet) | **Get** /repositories/{username}/{repo_slug}/components/{component_id} | 
[**RepositoriesUsernameRepoSlugComponentsGet**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugComponentsGet) | **Get** /repositories/{username}/{repo_slug}/components | 
[**RepositoriesUsernameRepoSlugIssuesGet**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugIssuesGet) | **Get** /repositories/{username}/{repo_slug}/issues | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsGet**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsGet) | **Get** /repositories/{username}/{repo_slug}/issues/{issue_id}/attachments | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathDelete**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathDelete) | **Delete** /repositories/{username}/{repo_slug}/issues/{issue_id}/attachments/{path} | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathGet**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathGet) | **Get** /repositories/{username}/{repo_slug}/issues/{issue_id}/attachments/{path} | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPost**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPost) | **Post** /repositories/{username}/{repo_slug}/issues/{issue_id}/attachments | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentIdGet**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentIdGet) | **Get** /repositories/{username}/{repo_slug}/issues/{issue_id}/comments/{comment_id} | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdCommentsGet**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugIssuesIssueIdCommentsGet) | **Get** /repositories/{username}/{repo_slug}/issues/{issue_id}/comments | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdDelete**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugIssuesIssueIdDelete) | **Delete** /repositories/{username}/{repo_slug}/issues/{issue_id} | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdGet**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugIssuesIssueIdGet) | **Get** /repositories/{username}/{repo_slug}/issues/{issue_id} | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdVoteDelete**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugIssuesIssueIdVoteDelete) | **Delete** /repositories/{username}/{repo_slug}/issues/{issue_id}/vote | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdVoteGet**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugIssuesIssueIdVoteGet) | **Get** /repositories/{username}/{repo_slug}/issues/{issue_id}/vote | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdVotePut**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugIssuesIssueIdVotePut) | **Put** /repositories/{username}/{repo_slug}/issues/{issue_id}/vote | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdWatchDelete**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugIssuesIssueIdWatchDelete) | **Delete** /repositories/{username}/{repo_slug}/issues/{issue_id}/watch | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdWatchGet**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugIssuesIssueIdWatchGet) | **Get** /repositories/{username}/{repo_slug}/issues/{issue_id}/watch | 
[**RepositoriesUsernameRepoSlugIssuesIssueIdWatchPut**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugIssuesIssueIdWatchPut) | **Put** /repositories/{username}/{repo_slug}/issues/{issue_id}/watch | 
[**RepositoriesUsernameRepoSlugIssuesPost**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugIssuesPost) | **Post** /repositories/{username}/{repo_slug}/issues | 
[**RepositoriesUsernameRepoSlugMilestonesGet**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugMilestonesGet) | **Get** /repositories/{username}/{repo_slug}/milestones | 
[**RepositoriesUsernameRepoSlugMilestonesMilestoneIdGet**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugMilestonesMilestoneIdGet) | **Get** /repositories/{username}/{repo_slug}/milestones/{milestone_id} | 
[**RepositoriesUsernameRepoSlugVersionsGet**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugVersionsGet) | **Get** /repositories/{username}/{repo_slug}/versions | 
[**RepositoriesUsernameRepoSlugVersionsVersionIdGet**](IssuetrackerApi.md#RepositoriesUsernameRepoSlugVersionsVersionIdGet) | **Get** /repositories/{username}/{repo_slug}/versions/{version_id} | 


# **RepositoriesUsernameRepoSlugComponentsComponentIdGet**
> Component RepositoriesUsernameRepoSlugComponentsComponentIdGet($username, $componentId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **componentId** | **int32**| The component&#39;s id | 

### Return type

[**Component**](component.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugComponentsGet**
> PaginatedComponents RepositoriesUsernameRepoSlugComponentsGet($username, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**PaginatedComponents**](paginated_components.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugIssuesGet**
> PaginatedIssues RepositoriesUsernameRepoSlugIssuesGet($username, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**PaginatedIssues**](paginated_issues.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsGet**
> PaginatedIssueAttachments RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsGet($username, $issueId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **issueId** | **int32**| The issue&#39;s id | 

### Return type

[**PaginatedIssueAttachments**](paginated_issue_attachments.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathDelete**
> RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathDelete($username, $path, $issueId, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **path** | **string**|  | 
 **issueId** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathGet**
> RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathGet($username, $path, $issueId, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **path** | **string**|  | 
 **issueId** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPost**
> RepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPost($username, $issueId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **issueId** | **int32**| The issue&#39;s id | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentIdGet**
> ModelError RepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentIdGet($username, $commentId, $issueId, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **commentId** | **string**|  | 
 **issueId** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugIssuesIssueIdCommentsGet**
> ModelError RepositoriesUsernameRepoSlugIssuesIssueIdCommentsGet($username, $issueId, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **issueId** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugIssuesIssueIdDelete**
> Issue RepositoriesUsernameRepoSlugIssuesIssueIdDelete($username, $issueId, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **issueId** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**Issue**](issue.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugIssuesIssueIdGet**
> Issue RepositoriesUsernameRepoSlugIssuesIssueIdGet($username, $issueId, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **issueId** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**Issue**](issue.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugIssuesIssueIdVoteDelete**
> ModelError RepositoriesUsernameRepoSlugIssuesIssueIdVoteDelete($username, $issueId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **issueId** | **int32**| The issue&#39;s id | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugIssuesIssueIdVoteGet**
> ModelError RepositoriesUsernameRepoSlugIssuesIssueIdVoteGet($username, $issueId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **issueId** | **int32**| The issue&#39;s id | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugIssuesIssueIdVotePut**
> ModelError RepositoriesUsernameRepoSlugIssuesIssueIdVotePut($username, $issueId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **issueId** | **int32**| The issue&#39;s id | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugIssuesIssueIdWatchDelete**
> ModelError RepositoriesUsernameRepoSlugIssuesIssueIdWatchDelete($username, $issueId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **issueId** | **int32**| The issue&#39;s id | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugIssuesIssueIdWatchGet**
> ModelError RepositoriesUsernameRepoSlugIssuesIssueIdWatchGet($username, $issueId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **issueId** | **int32**| The issue&#39;s id | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugIssuesIssueIdWatchPut**
> ModelError RepositoriesUsernameRepoSlugIssuesIssueIdWatchPut($username, $issueId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **issueId** | **int32**| The issue&#39;s id | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugIssuesPost**
> Issue RepositoriesUsernameRepoSlugIssuesPost($username, $repoSlug, $body)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 
 **body** | [**Issue**](Issue.md)| The new issue. Note that the only required element is &#x60;title&#x60;. All other elements can be omitted from the body. | 

### Return type

[**Issue**](issue.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugMilestonesGet**
> PaginatedMilestones RepositoriesUsernameRepoSlugMilestonesGet($username, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**PaginatedMilestones**](paginated_milestones.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugMilestonesMilestoneIdGet**
> Milestone RepositoriesUsernameRepoSlugMilestonesMilestoneIdGet($username, $milestoneId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **milestoneId** | **int32**| The milestone&#39;s id | 

### Return type

[**Milestone**](milestone.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugVersionsGet**
> PaginatedVersions RepositoriesUsernameRepoSlugVersionsGet($username, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**PaginatedVersions**](paginated_versions.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugVersionsVersionIdGet**
> Version RepositoriesUsernameRepoSlugVersionsVersionIdGet($username, $versionId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **versionId** | **int32**| The version&#39;s id | 

### Return type

[**Version**](version.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

