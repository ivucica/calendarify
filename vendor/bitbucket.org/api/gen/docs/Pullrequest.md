# Pullrequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type_** | **string** |  | [default to null]
**State** | **string** |  | [optional] [default to null]
**Author** | [**Account**](account.md) |  | [optional] [default to null]
**Id** | **int32** |  | [optional] [default to null]
**Links** | [**Object**](object.md) |  | [optional] [default to null]
**Title** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


