# \PullrequestsApi

All URIs are relative to *https://api.bitbucket.org/2.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**RepositoriesUsernameRepoSlugDefaultReviewersGet**](PullrequestsApi.md#RepositoriesUsernameRepoSlugDefaultReviewersGet) | **Get** /repositories/{username}/{repo_slug}/default-reviewers | 
[**RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameDelete**](PullrequestsApi.md#RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameDelete) | **Delete** /repositories/{username}/{repo_slug}/default-reviewers/{target_username} | 
[**RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameGet**](PullrequestsApi.md#RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameGet) | **Get** /repositories/{username}/{repo_slug}/default-reviewers/{target_username} | 
[**RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernamePut**](PullrequestsApi.md#RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernamePut) | **Put** /repositories/{username}/{repo_slug}/default-reviewers/{target_username} | 
[**RepositoriesUsernameRepoSlugPullrequestsActivityGet**](PullrequestsApi.md#RepositoriesUsernameRepoSlugPullrequestsActivityGet) | **Get** /repositories/{username}/{repo_slug}/pullrequests/activity | 
[**RepositoriesUsernameRepoSlugPullrequestsGet**](PullrequestsApi.md#RepositoriesUsernameRepoSlugPullrequestsGet) | **Get** /repositories/{username}/{repo_slug}/pullrequests | 
[**RepositoriesUsernameRepoSlugPullrequestsPost**](PullrequestsApi.md#RepositoriesUsernameRepoSlugPullrequestsPost) | **Post** /repositories/{username}/{repo_slug}/pullrequests | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivityGet**](PullrequestsApi.md#RepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivityGet) | **Get** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/activity | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdApproveDelete**](PullrequestsApi.md#RepositoriesUsernameRepoSlugPullrequestsPullRequestIdApproveDelete) | **Delete** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/approve | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprovePost**](PullrequestsApi.md#RepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprovePost) | **Post** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/approve | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentIdGet**](PullrequestsApi.md#RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentIdGet) | **Get** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/comments/{comment_id} | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsGet**](PullrequestsApi.md#RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsGet) | **Get** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/comments | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommitsGet**](PullrequestsApi.md#RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommitsGet) | **Get** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/commits | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdDeclinePost**](PullrequestsApi.md#RepositoriesUsernameRepoSlugPullrequestsPullRequestIdDeclinePost) | **Post** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/decline | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiffGet**](PullrequestsApi.md#RepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiffGet) | **Get** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/diff | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdGet**](PullrequestsApi.md#RepositoriesUsernameRepoSlugPullrequestsPullRequestIdGet) | **Get** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id} | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdMergePost**](PullrequestsApi.md#RepositoriesUsernameRepoSlugPullrequestsPullRequestIdMergePost) | **Post** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/merge | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatchGet**](PullrequestsApi.md#RepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatchGet) | **Get** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/patch | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdPut**](PullrequestsApi.md#RepositoriesUsernameRepoSlugPullrequestsPullRequestIdPut) | **Put** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id} | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatusesGet**](PullrequestsApi.md#RepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatusesGet) | **Get** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/statuses | 


# **RepositoriesUsernameRepoSlugDefaultReviewersGet**
> RepositoriesUsernameRepoSlugDefaultReviewersGet($username, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameDelete**
> ModelError RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameDelete($username, $targetUsername, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **targetUsername** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameGet**
> ModelError RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameGet($username, $targetUsername, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **targetUsername** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernamePut**
> ModelError RepositoriesUsernameRepoSlugDefaultReviewersTargetUsernamePut($username, $targetUsername, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **targetUsername** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugPullrequestsActivityGet**
> ModelError RepositoriesUsernameRepoSlugPullrequestsActivityGet($username, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugPullrequestsGet**
> ModelError RepositoriesUsernameRepoSlugPullrequestsGet($username, $repoSlug, $state)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 
 **state** | **string**| Only return pull requests that in this state. This parameter can be repeated. | [optional] 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugPullrequestsPost**
> Pullrequest RepositoriesUsernameRepoSlugPullrequestsPost($username, $repoSlug, $body)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 
 **body** | [**Pullrequest**](Pullrequest.md)| The new pull request.  The request URL you POST to becomes the destination repository URL. For this reason, you must specify an explicit source repository in the request object if you want to pull from a different repository (fork).  Since not all elements are required or even mutable, you only need to include the elements you want to initialize, such as the source branch and the title. | [optional] 

### Return type

[**Pullrequest**](pullrequest.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivityGet**
> ModelError RepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivityGet($username, $pullRequestId, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pullRequestId** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdApproveDelete**
> ModelError RepositoriesUsernameRepoSlugPullrequestsPullRequestIdApproveDelete($username, $pullRequestId, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pullRequestId** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprovePost**
> ModelError RepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprovePost($username, $pullRequestId, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pullRequestId** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentIdGet**
> ModelError RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentIdGet($username, $pullRequestId, $commentId, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pullRequestId** | **string**|  | 
 **commentId** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsGet**
> ModelError RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsGet($username, $pullRequestId, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pullRequestId** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommitsGet**
> ModelError RepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommitsGet($username, $pullRequestId, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pullRequestId** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdDeclinePost**
> Pullrequest RepositoriesUsernameRepoSlugPullrequestsPullRequestIdDeclinePost($username, $pullRequestId, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pullRequestId** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**Pullrequest**](pullrequest.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiffGet**
> ModelError RepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiffGet($username, $pullRequestId, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pullRequestId** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdGet**
> Pullrequest RepositoriesUsernameRepoSlugPullrequestsPullRequestIdGet($username, $pullRequestId, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pullRequestId** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**Pullrequest**](pullrequest.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdMergePost**
> Pullrequest RepositoriesUsernameRepoSlugPullrequestsPullRequestIdMergePost($username, $pullRequestId, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pullRequestId** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**Pullrequest**](pullrequest.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatchGet**
> ModelError RepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatchGet($username, $pullRequestId, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pullRequestId** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdPut**
> ModelError RepositoriesUsernameRepoSlugPullrequestsPullRequestIdPut($username, $pullRequestId, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pullRequestId** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatusesGet**
> PaginatedCommitstatuses RepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatusesGet($username, $pullRequestId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pullRequestId** | **int32**| The pull request&#39;s id | 

### Return type

[**PaginatedCommitstatuses**](paginated_commitstatuses.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

