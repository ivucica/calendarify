# \RefsApi

All URIs are relative to *https://api.bitbucket.org/2.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**RepositoriesUsernameRepoSlugRefsBranchesGet**](RefsApi.md#RepositoriesUsernameRepoSlugRefsBranchesGet) | **Get** /repositories/{username}/{repo_slug}/refs/branches | 
[**RepositoriesUsernameRepoSlugRefsBranchesNameGet**](RefsApi.md#RepositoriesUsernameRepoSlugRefsBranchesNameGet) | **Get** /repositories/{username}/{repo_slug}/refs/branches/{name} | 
[**RepositoriesUsernameRepoSlugRefsGet**](RefsApi.md#RepositoriesUsernameRepoSlugRefsGet) | **Get** /repositories/{username}/{repo_slug}/refs | 
[**RepositoriesUsernameRepoSlugRefsTagsGet**](RefsApi.md#RepositoriesUsernameRepoSlugRefsTagsGet) | **Get** /repositories/{username}/{repo_slug}/refs/tags | 
[**RepositoriesUsernameRepoSlugRefsTagsNameGet**](RefsApi.md#RepositoriesUsernameRepoSlugRefsTagsNameGet) | **Get** /repositories/{username}/{repo_slug}/refs/tags/{name} | 


# **RepositoriesUsernameRepoSlugRefsBranchesGet**
> ModelError RepositoriesUsernameRepoSlugRefsBranchesGet($username, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugRefsBranchesNameGet**
> ModelError RepositoriesUsernameRepoSlugRefsBranchesNameGet($username, $name, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **name** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugRefsGet**
> ModelError RepositoriesUsernameRepoSlugRefsGet($username, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugRefsTagsGet**
> ModelError RepositoriesUsernameRepoSlugRefsTagsGet($username, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugRefsTagsNameGet**
> ModelError RepositoriesUsernameRepoSlugRefsTagsNameGet($username, $name, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **name** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

