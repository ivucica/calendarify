# \RepositoriesApi

All URIs are relative to *https://api.bitbucket.org/2.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**RepositoriesGet**](RepositoriesApi.md#RepositoriesGet) | **Get** /repositories | 
[**RepositoriesUsernameGet**](RepositoriesApi.md#RepositoriesUsernameGet) | **Get** /repositories/{username} | 
[**RepositoriesUsernameRepoSlugCommitNodeStatusesBuildKeyGet**](RepositoriesApi.md#RepositoriesUsernameRepoSlugCommitNodeStatusesBuildKeyGet) | **Get** /repositories/{username}/{repo_slug}/commit/{node}/statuses/build/{key} | 
[**RepositoriesUsernameRepoSlugCommitNodeStatusesBuildKeyPut**](RepositoriesApi.md#RepositoriesUsernameRepoSlugCommitNodeStatusesBuildKeyPut) | **Put** /repositories/{username}/{repo_slug}/commit/{node}/statuses/build/{key} | 
[**RepositoriesUsernameRepoSlugCommitNodeStatusesBuildPost**](RepositoriesApi.md#RepositoriesUsernameRepoSlugCommitNodeStatusesBuildPost) | **Post** /repositories/{username}/{repo_slug}/commit/{node}/statuses/build | 
[**RepositoriesUsernameRepoSlugCommitNodeStatusesGet**](RepositoriesApi.md#RepositoriesUsernameRepoSlugCommitNodeStatusesGet) | **Get** /repositories/{username}/{repo_slug}/commit/{node}/statuses | 
[**RepositoriesUsernameRepoSlugDelete**](RepositoriesApi.md#RepositoriesUsernameRepoSlugDelete) | **Delete** /repositories/{username}/{repo_slug} | 
[**RepositoriesUsernameRepoSlugForksGet**](RepositoriesApi.md#RepositoriesUsernameRepoSlugForksGet) | **Get** /repositories/{username}/{repo_slug}/forks | 
[**RepositoriesUsernameRepoSlugGet**](RepositoriesApi.md#RepositoriesUsernameRepoSlugGet) | **Get** /repositories/{username}/{repo_slug} | 
[**RepositoriesUsernameRepoSlugHooksGet**](RepositoriesApi.md#RepositoriesUsernameRepoSlugHooksGet) | **Get** /repositories/{username}/{repo_slug}/hooks | 
[**RepositoriesUsernameRepoSlugHooksPost**](RepositoriesApi.md#RepositoriesUsernameRepoSlugHooksPost) | **Post** /repositories/{username}/{repo_slug}/hooks | 
[**RepositoriesUsernameRepoSlugHooksUidDelete**](RepositoriesApi.md#RepositoriesUsernameRepoSlugHooksUidDelete) | **Delete** /repositories/{username}/{repo_slug}/hooks/{uid} | 
[**RepositoriesUsernameRepoSlugHooksUidGet**](RepositoriesApi.md#RepositoriesUsernameRepoSlugHooksUidGet) | **Get** /repositories/{username}/{repo_slug}/hooks/{uid} | 
[**RepositoriesUsernameRepoSlugHooksUidPut**](RepositoriesApi.md#RepositoriesUsernameRepoSlugHooksUidPut) | **Put** /repositories/{username}/{repo_slug}/hooks/{uid} | 
[**RepositoriesUsernameRepoSlugPost**](RepositoriesApi.md#RepositoriesUsernameRepoSlugPost) | **Post** /repositories/{username}/{repo_slug} | 
[**RepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatusesGet**](RepositoriesApi.md#RepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatusesGet) | **Get** /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/statuses | 
[**RepositoriesUsernameRepoSlugPut**](RepositoriesApi.md#RepositoriesUsernameRepoSlugPut) | **Put** /repositories/{username}/{repo_slug} | 
[**RepositoriesUsernameRepoSlugWatchersGet**](RepositoriesApi.md#RepositoriesUsernameRepoSlugWatchersGet) | **Get** /repositories/{username}/{repo_slug}/watchers | 


# **RepositoriesGet**
> PaginatedRepositories RepositoriesGet()






### Parameters
This endpoint does not need any parameter.

### Return type

[**PaginatedRepositories**](paginated_repositories.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameGet**
> PaginatedRepositories RepositoriesUsernameGet($username, $role)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **role** | **string**|  Filters the result based on the authenticated user&#39;s role on each repository.  * **member**: returns repositories to which the user has explicit read access * **contributor**: returns repositories to which the user has explicit write access * **admin**: returns repositories to which the user has explicit administrator access * **owner**: returns all repositories owned by the current user  | [optional] 

### Return type

[**PaginatedRepositories**](paginated_repositories.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugCommitNodeStatusesBuildKeyGet**
> Commitstatus RepositoriesUsernameRepoSlugCommitNodeStatusesBuildKeyGet($username, $node, $key)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **node** | **string**| The commit&#39;s SHA1 | 
 **key** | **string**| The build status&#39; unique key | 

### Return type

[**Commitstatus**](commitstatus.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugCommitNodeStatusesBuildKeyPut**
> Commitstatus RepositoriesUsernameRepoSlugCommitNodeStatusesBuildKeyPut($username, $node, $key, $body)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **node** | **string**| The commit&#39;s SHA1 | 
 **key** | **string**| The commit status&#39; unique key | 
 **body** | [**Commitstatus**](Commitstatus.md)| The updated build status object | [optional] 

### Return type

[**Commitstatus**](commitstatus.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugCommitNodeStatusesBuildPost**
> Commitstatus RepositoriesUsernameRepoSlugCommitNodeStatusesBuildPost($username, $node, $key, $body)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **node** | **string**| The commit&#39;s SHA1 | 
 **key** | **string**| The commit status&#39; unique key | 
 **body** | [**Commitstatus**](Commitstatus.md)| The new commit status object. | [optional] 

### Return type

[**Commitstatus**](commitstatus.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugCommitNodeStatusesGet**
> PaginatedCommitstatuses RepositoriesUsernameRepoSlugCommitNodeStatusesGet($username, $node)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **node** | **string**| The commit&#39;s SHA1 | 

### Return type

[**PaginatedCommitstatuses**](paginated_commitstatuses.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugDelete**
> RepositoriesUsernameRepoSlugDelete($username, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugForksGet**
> PaginatedRepositories RepositoriesUsernameRepoSlugForksGet($username, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**PaginatedRepositories**](paginated_repositories.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugGet**
> Repository RepositoriesUsernameRepoSlugGet($username, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**Repository**](repository.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugHooksGet**
> PaginatedWebhookSubscriptions RepositoriesUsernameRepoSlugHooksGet($username, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**PaginatedWebhookSubscriptions**](paginated_webhook_subscriptions.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugHooksPost**
> WebhookSubscription RepositoriesUsernameRepoSlugHooksPost($username, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**WebhookSubscription**](webhook_subscription.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugHooksUidDelete**
> RepositoriesUsernameRepoSlugHooksUidDelete($username, $uid)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **uid** | **string**| The installed webhook&#39;s id | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugHooksUidGet**
> WebhookSubscription RepositoriesUsernameRepoSlugHooksUidGet($username, $uid)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **uid** | **string**| The installed webhook&#39;s id. | 

### Return type

[**WebhookSubscription**](webhook_subscription.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugHooksUidPut**
> WebhookSubscription RepositoriesUsernameRepoSlugHooksUidPut($username, $uid)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **uid** | **string**| The installed webhook&#39;s id | 

### Return type

[**WebhookSubscription**](webhook_subscription.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugPost**
> Repository RepositoriesUsernameRepoSlugPost($username, $repoSlug, $body)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 
 **body** | [**Repository**](Repository.md)| The repository that is to be created. Note that most object elements are optional. Elements \&quot;owner\&quot; and \&quot;full_name\&quot; are ignored as the URL implies them. | [optional] 

### Return type

[**Repository**](repository.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatusesGet**
> PaginatedCommitstatuses RepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatusesGet($username, $pullRequestId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **pullRequestId** | **int32**| The pull request&#39;s id | 

### Return type

[**PaginatedCommitstatuses**](paginated_commitstatuses.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugPut**
> Repository RepositoriesUsernameRepoSlugPut($username, $repoSlug, $body)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 
 **body** | [**Repository**](Repository.md)| The repository that is to be updated.  Note that the elements \&quot;owner\&quot; and \&quot;full_name\&quot; are ignored since the URL implies them.  | [optional] 

### Return type

[**Repository**](repository.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RepositoriesUsernameRepoSlugWatchersGet**
> ModelError RepositoriesUsernameRepoSlugWatchersGet($username, $repoSlug)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **repoSlug** | **string**|  | 

### Return type

[**ModelError**](error.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

