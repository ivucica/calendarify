# \SnippetsApi

All URIs are relative to *https://api.bitbucket.org/2.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**SnippetsGet**](SnippetsApi.md#SnippetsGet) | **Get** /snippets | 
[**SnippetsPost**](SnippetsApi.md#SnippetsPost) | **Post** /snippets | 
[**SnippetsUsernameEncodedIdCommentsCommentIdDelete**](SnippetsApi.md#SnippetsUsernameEncodedIdCommentsCommentIdDelete) | **Delete** /snippets/{username}/{encoded_id}/comments/{comment_id} | 
[**SnippetsUsernameEncodedIdCommentsCommentIdGet**](SnippetsApi.md#SnippetsUsernameEncodedIdCommentsCommentIdGet) | **Get** /snippets/{username}/{encoded_id}/comments/{comment_id} | 
[**SnippetsUsernameEncodedIdCommentsCommentIdPut**](SnippetsApi.md#SnippetsUsernameEncodedIdCommentsCommentIdPut) | **Put** /snippets/{username}/{encoded_id}/comments/{comment_id} | 
[**SnippetsUsernameEncodedIdCommentsGet**](SnippetsApi.md#SnippetsUsernameEncodedIdCommentsGet) | **Get** /snippets/{username}/{encoded_id}/comments | 
[**SnippetsUsernameEncodedIdCommentsPost**](SnippetsApi.md#SnippetsUsernameEncodedIdCommentsPost) | **Post** /snippets/{username}/{encoded_id}/comments | 
[**SnippetsUsernameEncodedIdCommitsGet**](SnippetsApi.md#SnippetsUsernameEncodedIdCommitsGet) | **Get** /snippets/{username}/{encoded_id}/commits | 
[**SnippetsUsernameEncodedIdCommitsRevisionGet**](SnippetsApi.md#SnippetsUsernameEncodedIdCommitsRevisionGet) | **Get** /snippets/{username}/{encoded_id}/commits/{revision} | 
[**SnippetsUsernameEncodedIdDelete**](SnippetsApi.md#SnippetsUsernameEncodedIdDelete) | **Delete** /snippets/{username}/{encoded_id} | 
[**SnippetsUsernameEncodedIdGet**](SnippetsApi.md#SnippetsUsernameEncodedIdGet) | **Get** /snippets/{username}/{encoded_id} | 
[**SnippetsUsernameEncodedIdNodeIdDelete**](SnippetsApi.md#SnippetsUsernameEncodedIdNodeIdDelete) | **Delete** /snippets/{username}/{encoded_id}/{node_id} | 
[**SnippetsUsernameEncodedIdNodeIdFilesPathGet**](SnippetsApi.md#SnippetsUsernameEncodedIdNodeIdFilesPathGet) | **Get** /snippets/{username}/{encoded_id}/{node_id}/files/{path} | 
[**SnippetsUsernameEncodedIdNodeIdGet**](SnippetsApi.md#SnippetsUsernameEncodedIdNodeIdGet) | **Get** /snippets/{username}/{encoded_id}/{node_id} | 
[**SnippetsUsernameEncodedIdNodeIdPut**](SnippetsApi.md#SnippetsUsernameEncodedIdNodeIdPut) | **Put** /snippets/{username}/{encoded_id}/{node_id} | 
[**SnippetsUsernameEncodedIdPut**](SnippetsApi.md#SnippetsUsernameEncodedIdPut) | **Put** /snippets/{username}/{encoded_id} | 
[**SnippetsUsernameEncodedIdRevisionDiffGet**](SnippetsApi.md#SnippetsUsernameEncodedIdRevisionDiffGet) | **Get** /snippets/{username}/{encoded_id}/{revision}/diff | 
[**SnippetsUsernameEncodedIdRevisionPatchGet**](SnippetsApi.md#SnippetsUsernameEncodedIdRevisionPatchGet) | **Get** /snippets/{username}/{encoded_id}/{revision}/patch | 
[**SnippetsUsernameEncodedIdWatchDelete**](SnippetsApi.md#SnippetsUsernameEncodedIdWatchDelete) | **Delete** /snippets/{username}/{encoded_id}/watch | 
[**SnippetsUsernameEncodedIdWatchGet**](SnippetsApi.md#SnippetsUsernameEncodedIdWatchGet) | **Get** /snippets/{username}/{encoded_id}/watch | 
[**SnippetsUsernameEncodedIdWatchPut**](SnippetsApi.md#SnippetsUsernameEncodedIdWatchPut) | **Put** /snippets/{username}/{encoded_id}/watch | 
[**SnippetsUsernameEncodedIdWatchersGet**](SnippetsApi.md#SnippetsUsernameEncodedIdWatchersGet) | **Get** /snippets/{username}/{encoded_id}/watchers | 
[**SnippetsUsernameGet**](SnippetsApi.md#SnippetsUsernameGet) | **Get** /snippets/{username} | 
[**SnippetsUsernamePost**](SnippetsApi.md#SnippetsUsernamePost) | **Post** /snippets/{username} | 


# **SnippetsGet**
> PaginatedSnippets SnippetsGet($role)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role** | **string**| Filter down the result based on the authenticated user&#39;s role (&#x60;owner&#x60;, &#x60;contributor&#x60;, or &#x60;member&#x60;). | [optional] 

### Return type

[**PaginatedSnippets**](paginated_snippets.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsPost**
> Snippet SnippetsPost($body)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Snippet**](Snippet.md)| The new snippet object. | 

### Return type

[**Snippet**](snippet.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernameEncodedIdCommentsCommentIdDelete**
> SnippetsUsernameEncodedIdCommentsCommentIdDelete($username, $commentId, $encodedId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **commentId** | **string**|  | 
 **encodedId** | **string**|  | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernameEncodedIdCommentsCommentIdGet**
> SnippetComment SnippetsUsernameEncodedIdCommentsCommentIdGet($username, $commentId, $encodedId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **commentId** | **string**|  | 
 **encodedId** | **string**|  | 

### Return type

[**SnippetComment**](snippet_comment.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernameEncodedIdCommentsCommentIdPut**
> SnippetsUsernameEncodedIdCommentsCommentIdPut($username, $commentId, $encodedId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **commentId** | **string**|  | 
 **encodedId** | **string**|  | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernameEncodedIdCommentsGet**
> PaginatedSnippetComments SnippetsUsernameEncodedIdCommentsGet($username, $encodedId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **encodedId** | **string**|  | 

### Return type

[**PaginatedSnippetComments**](paginated_snippet_comments.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernameEncodedIdCommentsPost**
> Snippet SnippetsUsernameEncodedIdCommentsPost($username, $encodedId, $body)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **encodedId** | **string**|  | 
 **body** | [**Snippet**](Snippet.md)| The contents of the new comment. | 

### Return type

[**Snippet**](snippet.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernameEncodedIdCommitsGet**
> PaginatedSnippetCommit SnippetsUsernameEncodedIdCommitsGet($username, $encodedId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **encodedId** | **string**|  | 

### Return type

[**PaginatedSnippetCommit**](paginated_snippet_commit.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernameEncodedIdCommitsRevisionGet**
> SnippetCommit SnippetsUsernameEncodedIdCommitsRevisionGet($username, $encodedId, $revision)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **encodedId** | **string**|  | 
 **revision** | **string**|  | 

### Return type

[**SnippetCommit**](snippet_commit.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernameEncodedIdDelete**
> SnippetsUsernameEncodedIdDelete($username, $encodedId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **encodedId** | **string**| The snippet&#39;s id. | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernameEncodedIdGet**
> Snippet SnippetsUsernameEncodedIdGet($username, $encodedId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **encodedId** | **string**| The snippet&#39;s id. | 

### Return type

[**Snippet**](snippet.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, multipart/related, multipart/form-data

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernameEncodedIdNodeIdDelete**
> SnippetsUsernameEncodedIdNodeIdDelete($username, $nodeId, $encodedId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **nodeId** | **string**|  | 
 **encodedId** | **string**| The snippet&#39;s id. | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernameEncodedIdNodeIdFilesPathGet**
> SnippetsUsernameEncodedIdNodeIdFilesPathGet($username, $path, $nodeId, $encodedId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **path** | **string**|  | 
 **nodeId** | **string**|  | 
 **encodedId** | **string**|  | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernameEncodedIdNodeIdGet**
> Snippet SnippetsUsernameEncodedIdNodeIdGet($username, $encodedId, $nodeId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **encodedId** | **string**| The snippet&#39;s id. | 
 **nodeId** | **string**| A commit revision (SHA1). | 

### Return type

[**Snippet**](snippet.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, multipart/related, multipart/form-data

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernameEncodedIdNodeIdPut**
> Snippet SnippetsUsernameEncodedIdNodeIdPut($username, $encodedId, $nodeId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **encodedId** | **string**| The snippet&#39;s id. | 
 **nodeId** | **string**| A commit revision (SHA1). | 

### Return type

[**Snippet**](snippet.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json, multipart/related, multipart/form-data
 - **Accept**: application/json, multipart/related, multipart/form-data

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernameEncodedIdPut**
> Snippet SnippetsUsernameEncodedIdPut($username, $encodedId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **encodedId** | **string**| The snippet&#39;s id. | 

### Return type

[**Snippet**](snippet.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json, multipart/related, multipart/form-data
 - **Accept**: application/json, multipart/related, multipart/form-data

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernameEncodedIdRevisionDiffGet**
> SnippetsUsernameEncodedIdRevisionDiffGet($username, $encodedId, $revision, $path)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **encodedId** | **string**| The snippet id. | 
 **revision** | **string**| A revspec expression. This can simply be a commit SHA1, a ref name, or a compare expression like &#x60;staging..production&#x60;. | 
 **path** | **string**| When used, only one the diff of the specified file will be returned. | [optional] 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernameEncodedIdRevisionPatchGet**
> SnippetsUsernameEncodedIdRevisionPatchGet($username, $encodedId, $revision)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **encodedId** | **string**| The snippet id. | 
 **revision** | **string**| A revspec expression. This can simply be a commit SHA1, a ref name, or a compare expression like &#x60;staging..production&#x60;. | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernameEncodedIdWatchDelete**
> PaginatedUsers SnippetsUsernameEncodedIdWatchDelete($username, $encodedId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **encodedId** | **string**| The snippet id. | 

### Return type

[**PaginatedUsers**](paginated_users.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernameEncodedIdWatchGet**
> PaginatedUsers SnippetsUsernameEncodedIdWatchGet($username, $encodedId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **encodedId** | **string**| The snippet id. | 

### Return type

[**PaginatedUsers**](paginated_users.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernameEncodedIdWatchPut**
> PaginatedUsers SnippetsUsernameEncodedIdWatchPut($username, $encodedId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **encodedId** | **string**| The snippet id. | 

### Return type

[**PaginatedUsers**](paginated_users.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernameEncodedIdWatchersGet**
> PaginatedUsers SnippetsUsernameEncodedIdWatchersGet($username, $encodedId)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **encodedId** | **string**| The snippet id. | 

### Return type

[**PaginatedUsers**](paginated_users.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernameGet**
> PaginatedSnippets SnippetsUsernameGet($username, $role)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| Limits the result to snippets owned by this user. | 
 **role** | **string**| Filter down the result based on the authenticated user&#39;s role (&#x60;owner&#x60;, &#x60;contributor&#x60;, or &#x60;member&#x60;). | [optional] 

### Return type

[**PaginatedSnippets**](paginated_snippets.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SnippetsUsernamePost**
> Snippet SnippetsUsernamePost($username, $body)






### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**|  | 
 **body** | [**Snippet**](Snippet.md)| The new snippet object. | 

### Return type

[**Snippet**](snippet.md)

### Authorization

[api_key](../README.md#api_key), [oauth2](../README.md#oauth2), [basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

