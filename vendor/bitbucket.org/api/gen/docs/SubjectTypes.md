# SubjectTypes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**User** | [**SubjectTypesUser**](subject_types_user.md) |  | [optional] [default to null]
**Repository** | [**SubjectTypesUser**](subject_types_user.md) |  | [optional] [default to null]
**Team** | [**SubjectTypesUser**](subject_types_user.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


