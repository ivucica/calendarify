set -e
DEST=$(bazel info workspace)/vendor/bitbucket.org/api/gen
bazel build //vendor/bitbucket.org/api:bitbucket.go.tar.gz

rm -rf ${DEST}
mkdir -p ${DEST}
cp $(bazel info bazel-genfiles)/vendor/bitbucket.org/api/bitbucket.go.tar.gz ${DEST}/bitbucket.go.tar.gz
( cd ${DEST} && tar xvvfz bitbucket.go.tar.gz && mv -v bitbucket.go/* . && rm -rf bitbucket.go )
