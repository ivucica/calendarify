#!/bin/bash

# Alternative:
# use http_file(name="bitbucket_swagger", url="https://bitbucket.org/api/swagger.json", sha256="...") in workspace
# then refer to the api as @bitbucket_swagger//file
curl https://bitbucket.org/api/swagger.json > swagger.json
